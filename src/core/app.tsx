import * as React from 'react';
import './app.css';

class App extends React.Component<{}, {}> {
  render() {
    return (
      <div className="root">
        <nav>
          <div className="transparent"></div>
          <div className="container">
            <a className="logo">Greetir</a>
          </div>
        </nav>
      </div>
    );
  }
}

export default App;
